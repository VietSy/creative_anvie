$(document).ready(function(){

	//Loading
	setTimeout(function(){
		$('.l-loader__img').html('<img src="/assets/img/common/loading.gif" alt="">');
	}, 300);
	$('.l-loader').delay(3200).fadeOut(500);

	//Slider
	$('.js-slider').slick({
		infinite: true,
		dots: false,
		arrows: true,
		slidesToScroll: 1,
		focusOnSelect: false,
		prevArrow: $('.c-slider__prev'),
		nextArrow: $('.c-slider__next'),
	});

	//Tabs
	$('.c-tabs__navi li').click(function(){
		var tab_id = $(this).attr('data');

		$('.c-tabs__navi li').removeClass('is-active');
		$('.c-tabs__cont').removeClass('is-active');

		$(this).addClass('is-active');
		$("."+tab_id).addClass('is-active');
	});

	//Animation04
	// setInterval(function(){
	// 	$('.c-animation04__inner01').toggleClass('is-active');
	// }, 3000);
	// setInterval(function(){
	// 	$('.c-animation04__inner02').toggleClass('is-active');
	// }, 4000);

	$('.js-animation04 .js-effect:gt(0)').removeClass('is-active');
	setInterval(function(){
		$('.js-animation04 .js-effect:first-child').removeClass('is-active')
		.next('.js-effect').addClass('is-active')
		.end().appendTo('.js-animation04');
	}, 3000);


	$('a[href^="#"]').click(function() {
        var speed = 600; 
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var hmenu = $('.c-header').height();
        var position = target.offset().top - hmenu
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });
});

$(window).on('beforeunload', function(){
	$(window).scrollTop(0);
});

function isViewItem(elem) {
    if ($(elem).length == 0) {
        return false;
    }
    var hwindown = "25vh";
    var itemViewTop = $(window).scrollTop();
    var itemViewBottom = itemViewTop + ($(window).height()/1.1);

    var itemTop = $(elem).offset().top;
    var itemBottom = itemTop + ($(elem).height()/1.1);
    return (itemViewBottom >= itemTop && itemViewTop <= itemBottom);
}

$(window).on("load scroll",function(e){
    $('.js-view').each(function () {
        if (isViewItem(this) === true) {
            $(this).addClass('is-view');
        }
    });
});