<footer class="c-footer">
	<div class="c-footer__nav">
		<a href="" class="c-footer__link">運営会社</a>
		<a href="" class="c-footer__license">License</a>
	</div>
	<p class="c-footer__copyright"><small>© 2019 Anvie.inc</small></p>
</footer>


<script src="/assets/js/slick.js"></script>
<script src="/assets/js/functions.min.js"></script>
</body>
</html>