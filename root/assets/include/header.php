<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" media="all">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">

<header class="c-header">
	<h1 class="c-header__logo"><img src="/assets/img/common/logo.svg" alt=""></h1>
	<a href="#contact" class="c-header__link">お問い合わせ</a>
</header><!-- /header -->
<div class="l-loader">
	<div class="l-loader__img"></div>
	<div class="l-loader__text">
		<p class="l-loader__txtEN">Update Legacy</p>
		<p class="l-loader__txtJA">レガシーをクリエイティブで塗り替える</p>
	</div>
</div>