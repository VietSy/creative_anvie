<div class="c-mv">
	<figure class="c-mv__head"><img src="/assets/img/common/mv_img01.svg" alt=""></figure>
	<figure class="c-mv__obj"><img src="/assets/img/common/mv_img02.svg" alt=""></figure>
	<div class="c-mv__text">
		<h2 class="c-mv__ttl">High performance<br>web creative</h2>
		<p class="c-mv__txt">企業のデジタル戦略において、<br>WEBのクリエイティブで圧倒的な成果を。</p>
	</div>
	<div class="c-mv__ani">
		<div class="c-mv__animation">			
			<div class="c-animation01">
				<img src="/assets/img/common/mv.svg" alt="">
				<ul class="c-animation01__inner01">
					<li><img src="/assets/img/common/mv_01.svg" alt=""></li>
					<li><img src="/assets/img/common/mv_02.svg" alt=""></li>
				</ul>
				<ul class="c-animation01__inner02">
					<li><img src="/assets/img/common/mv_03.svg" alt=""></li>
					<li><img src="/assets/img/common/mv_04.svg" alt=""></li>
					<li><img src="/assets/img/common/mv_05.svg" alt=""></li>
				</ul>
				<div class="c-animation01__inner03">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="c-animation01__inner04">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span><svg viewBox="0 0 32 32"><circle r="16" cx="16" cy="16"/></svg></span>
					<span><svg viewBox="0 0 50 50"><circle r="25" cx="25" cy="25"/></svg></span>
				</div>
				<div class="c-animation01__inner05">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="c-animation01__inner06">
					<span><img src="/assets/img/common/mv_07.svg" alt=""></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="c-animation01__inner07">
					<img src="/assets/img/common/mv_08.svg" alt="">
				</div>
				<div class="c-animation01__inner08">
					<img src="/assets/img/common/mv_09.svg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>