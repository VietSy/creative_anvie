<?php $pageid="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-top">
	<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/mv.php'); ?>
	<div class="p-top01">
		<div class="l-container">
			<div class="c-title01">
				<span class="c-title01__en">Why us</span>
				<h3 class="c-title01__txt"><span>ANVIE</span>が選ばれる理由</h3>
			</div>			
		</div>
		<div class="p-top01__slider">
			<div class="c-slider">
				<div class="c-slider__box js-slider">
					<div class="c-slider__item">
						<div class="c-slider__text">
							<h3 class="c-slider__ttl">美しく、整然としたデザイン</h3>
							<p class="c-slider__txt">私たちの制作するサイトは、整っています。 美しく、表現性があり、人々を魅了します。 どんなにマーケティングされたコンテンツだとしても、整然としていなければ効果は出ません。美しく、整然としていることがプロの仕事として最も大切のことです。</p>
						</div>
						<figure class="c-slider__icon c-slider__icon--01"></figure>
					</div>
					<div class="c-slider__item">
						<div class="c-slider__text">
							<h3 class="c-slider__ttl">成果にコミットする</h3>
							<p class="c-slider__txt">私たちは、名刺代わりのWebサイトではなく、 企業のデジタル戦略の一部として位置づけられる Webサイトを制作します。独りよがりな自己満足ではなく、プロフェッショナルとして高い成果をあげるために最善を尽くします。</p>
						</div>
						<figure class="c-slider__icon c-slider__icon--02"></figure>
					</div>
					<div class="c-slider__item">
						<div class="c-slider__text">
							<h3 class="c-slider__ttl">豊富な知見とロジカルな提案力</h3>
							<p class="c-slider__txt">GMO・DeNA・リクルートなどの大手IT企業出身の実績あるコンサルタントで構成されており、最先端で豊富な知見とロジカルな提案力で、プロジェクトをリードします。Web制作では進行管理の品質がサイトの品質に直結するので、私たちは徹底してお客様に伴走します。</p>
						</div>
						<figure class="c-slider__icon c-slider__icon--03"></figure>
					</div>
					<div class="c-slider__item">
						<div class="c-slider__text">
							<h3 class="c-slider__ttl">成果にコミットする</h3>
							<p class="c-slider__txt">私たちは、名刺代わりのWebサイトではなく、 企業のデジタル戦略の一部として位置づけられる Webサイトを制作します。独りよがりな自己満足ではなく、プロフェッショナルとして高い成果をあげるために最善を尽くします。</p>
						</div>
						<figure class="c-slider__icon c-slider__icon--02"></figure>
					</div>
				</div>
				<div class="c-slider__arrow c-slider__prev"></div>
				<div class="c-slider__arrow c-slider__next"></div>
			</div>
		</div>
	</div>
	<div class="p-top02">
		<div class="c-title01 c-title01--center js-view js-title">
			<span class="c-title01__en">What we can do</span>
			<h3 class="c-title01__txt">成果が上がるデザイン</h3>
		</div>
		<div class="p-top02__inner">
			<div class="c-block c-block--area01">
				<div class="c-block__item js-view js-fade">
					<div class="c-block__animation">
						<img src="/assets/img/common/animation01.svg" alt="">
					</div>
				</div>
				<div class="c-block__item">
					<div class="c-title02">
						<span class="c-title02__number c-title02__number--blue">01</span>
						<span class="c-title02__en">Animate illustration</span>
						<span class="c-title02__ja">アニメートイラストレーション</span>
					</div>
					<p class="c-block__txt">
						私たちはアイソメトリックなイラストや2Dイラストを<br>
						<strong>アニメーション</strong>させることで、さらに表現の幅を拡張します。 <br>
						AI領域・fintech領域など従来の平面的なイラストでは表現できなかったことを表現するのに適しています。<br>
						さらに、見るユーザーは貴社のサービスや商品、または世界観を直感的に感じることができ、エンゲージメント向上およびブランディングに大いに役立ちます。
					</p>
				</div>
			</div>
			<div class="c-block c-block--area02">
				<div class="c-block__item js-view js-fade">
					<div class="c-block__animation">
						<img src="/assets/img/common/animation02.svg" alt="">
					</div>
				</div>
				<div class="c-block__item">
					<div class="c-title02">
						<span class="c-title02__number c-title02__number--yellow">02</span>
						<span class="c-title02__en">Micro interaction</span>
						<span class="c-title02__ja">マイクロインタラクション</span>
					</div>
					<p class="c-block__txt">
						あなたが本当に伝えたいことに注意を引き、<br>
						興味を逃さないことを目的としたマイクロアニメーションです。<br>
						ユーザーのアクションに対して反射的に発火するため、<br>
						あなたが訴求したい内容にユーザーの注意を引くことができます。<br>
						また、1秒以内に完結するカジュアルなアニメーションのため、<br>
						ユーザーの興味を逃さないためテキストの読了率が向上します。
					</p>
				</div>
			</div>
			<div class="c-block c-block--area03">
				<div class="c-block__item js-view js-fade">
					<div class="c-block__animation">
						<div class="c-animation04">
							<img src="/assets/img/common/ani04_bg.svg" alt="">
							<div class="js-animation04">
								<ul class="c-animation04__img js-effect">
									<li><img src="/assets/img/common/ani04_01.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_02.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_03.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_04.svg" alt=""></li>
								</ul>
								<ul class="c-animation04__img js-effect">
									<li><img src="/assets/img/common/ani04_05.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_06.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_07.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_08.svg" alt=""></li>
								</ul>
								<ul class="c-animation04__img js-effect">
									<li><img src="/assets/img/common/ani04_09.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_10.svg" alt=""></li>
									<li><img src="/assets/img/common/ani04_11.svg" alt=""></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="c-block__item">
					<div class="c-title02">
						<span class="c-title02__number c-title02__number--green">03</span>
						<span class="c-title02__en">Seamless Transition</span>
						<span class="c-title02__ja">高速・シームレスなページ遷移</span>
					</div>
					<p class="c-block__txt">
						ページの表示速度を高速化することはどんなWebサイトにも重要です。<br>
						非同期通信技術を使用する事により、ページ遷移はより高速そしてシームレスにすることが可能になりました。<br>
						特にお問い合わせフォームのような入力、確認で出戻りをするコンテンツでは、一瞬でコンテンツが切り替わるため大きくユーザー体験を向上させます。
					</p>
				</div>
			</div>
		</div>
		<p class="p-top02__txt js-view js-fade">ANVIEは、熱狂的に細部にこだわり、<br class="sp-pnly">クリエイティブで成果を追求します。<br>業界最大のコストパフォーマンスにコミットします。</p>		
	</div>
	<div class="c-btn01">
		<a href="#contact">
			<span class="c-btn01__en">Contact</span>
			<span class="c-btn01__ja">お気軽にお問い合わせください。</span>
		</a>
	</div>
	<div class="p-top03">
		<div class="l-container">
			<div class="c-title01 c-title01--center js-view js-title">
				<span class="c-title01__en">Our thoughts</span>
				<h3 class="c-title01__txt">デザインにかける想い</h3>
			</div>
			<ul class="c-list01 js-view js-fade">
				<li class="c-list01__item">
					<span class="c-list01__ttlEN c-list01__ttlEN--icon01">Catch up</span>
					<h3 class="c-list01__ttlJA">最新トレンドを<br>キャッチアップし続ける</h3>
					<p class="c-list01__txt">WEBデザインの寿命は、2〜4年と言われております。 そんな中、我々は時代の最先端を行く海外からデザインを輸入することで、 少しでも息の長いWEBサイトを提供することにこだわり続けております。</p>
				</li>
				<li class="c-list01__item">
					<span class="c-list01__ttlEN c-list01__ttlEN--icon02">Beautifully</span>
					<h3 class="c-list01__ttlJA">”直感的”で<br>わかる美しさにこだわる</h3>
					<p class="c-list01__txt">UI /UXが考えられていないサイトは、 見るひとに負担も時間もかけてしまいます。 そのため我々は、デザイン、アニメーションという技術力を生かしながら、 少しでも見る人が感じる負担感にの軽減させたいと考えております。</p>
				</li>
				<li class="c-list01__item">
					<span class="c-list01__ttlEN c-list01__ttlEN--icon03">Fussy</span>
					<h3 class="c-list01__ttlJA">熱狂的なまでに<br>細部にこだわる</h3>
					<p class="c-list01__txt">テクノロジーが発展し情報が手に入りやすくなった今、競合との差別化も難しくなってきました。そんな時代だからこそ、細部へのこだわりが、競合との差別化になると我々は確信しているからです。</p>
				</li>
			</ul>
		</div>
	</div>
	<div class="p-top04">
		<div class="l-container">
			<div class="c-title01 c-title01--center js-view js-title">
				<span class="c-title01__en">Case study</span>
				<h3 class="c-title01__txt">制作プラン・ケーススタディ</h3>
			</div>
			<div class="c-tabs">
				<ul class="c-tabs__navi">
					<li class="is-active" data="tab01">LP/キャンペーンサイト</li>
					<li data="tab02">コーポレート</li>
					<li data="tab03">サービスサイト</li>
				</ul>
				<div class="c-tabs__cont tab01 is-active">
					<p class="c-tabs__txt">企業のイベントやキャンペーン情報などを掲載するサイトを制作します。<br>細部に入れ込むアニメーションなどで資料請求やお問い合わせ数を向上させるシカケを取り込んだサイトを制作いたします。</p>
					<ul class="c-tabs__quote">
						<li>制作日程</li>
						<li>目安費用</li>
					</ul>
					<div class="c-list02">
						<div class="c-list02__item">
							<h3 class="c-list02__ttl">CASE1</h3>
							<ul class="c-list02__cate">
								<li>TOP</li>
								<li>お問い合わせ/完了</li>
							</ul>
							<p class="c-list02__txt">広告用のランディングページおよびお問い合わせフォームを想定しています。<br>さらに、効果を測定するためのアクセス解析ツールの対策も万全です。</p>
							<ul class="c-list02__quote">
								<li>1~2ヶ月</li>
								<li>60万〜</li>
							</ul>
						</div>
						<div class="c-list02__item">
							<h3 class="c-list02__ttl">CASE2</h3>
							<ul class="c-list02__cate">
								<li>TOP</li>
								<li>サービス・キャンペーン詳細</li>
								<li>会社概要</li>
								<li>プライバシーポリシー</li>
								<li>サイトマップ</li>
								<li>お問い合わせ/完了</li>
								<li>404</li>
							</ul>
							<p class="c-list02__txt">CMSがついたキャンペーンサイトです。<br>詳細なキャンペーンの情報の掲載と更新を可能にします</p>
							<ul class="c-list02__quote">
								<li>2~4ヶ月</li>
								<li>150万〜</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="c-tabs__cont tab02">
					<p class="c-tabs__txt">マーケティング視点で企業のブランディングに役立つコーポレートサイトを制作します。<br>Wordpressの管理画面、お問い合わせフォームも提供します。</p>
					<ul class="c-tabs__quote">
						<li>制作日程</li>
						<li>目安費用</li>
					</ul>
					<div class="c-list02">
						<div class="c-list02__item">
							<h3 class="c-list02__ttl">CASE1</h3>
							<ul class="c-list02__cate">
								<li>TOP</li>
								<li>サービス</li>
								<li>会社概要</li>
								<li>ニュース/詳細</li>
								<li>お問い合わせ/完了</li>
								<li>プライバシーポリシー</li>
								<li>サイトマップ</li>
								<li>404</li>
							</ul>
							<p class="c-list02__txt">もっともスタンダードなページ数のコーポレートサイト。<br>事業の紹介、会社概要ページ、CMSのニュース配信、お問い合わせフォームを想定しています。</p>
							<ul class="c-list02__quote">
								<li>2~4ヶ月</li>
								<li>150万〜</li>
							</ul>
						</div>
						<div class="c-list02__item">
							<h3 class="c-list02__ttl">CASE2</h3>
							<ul class="c-list02__cate">
								<li>TOP</li>
								<li>サービス/詳細</li>
								<li>会社概要</li>
								<li>リクルート/詳細</li>
								<li>ニュース/詳細</li>
								<li>お問い合わせ/完了</li>
								<li>プライバシーポリシー</li>
								<li>サイトマップ</li>
								<li>404</li>
							</ul>
							<p class="c-list02__txt">採用情報およびサービスページを充実させた企業サイトのフルパッケージ。採用情報のCMS、サービスのCMSそしてニュースのCMSという<br>CMSを多く使用し、貴社で多くのページを更新していけるWebサイトです。</p>
							<ul class="c-list02__quote">
								<li>3~4ヶ月</li>
								<li>230万〜</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="c-tabs__cont tab03">
					<p class="c-tabs__txt">企業の商品やサービスを紹介するサイトを制作します。<br>イラストやアニメーションを使い、貴社の商品を直感的にわかりやすくユーザーに伝えます。</p>
					<ul class="c-tabs__quote">
						<li>制作日程</li>
						<li>目安費用</li>
					</ul>
					<div class="c-list02">
						<div class="c-list02__item">
							<h3 class="c-list02__ttl">CASE1</h3>
							<ul class="c-list02__cate">
								<li>TOP</li>
								<li>お問い合わせ/完了</li>
							</ul>
							<p class="c-list02__txt">TOPページとお問い合わせフォームを想定したサービスサイト。<br>情報設計が肝となるため、コンテンツの設計からご提案させていただきます。</p>
							<ul class="c-list02__quote">
								<li>1~2ヶ月</li>
								<li>60万〜</li>
							</ul>
						</div>
						<div class="c-list02__item">
							<h3 class="c-list02__ttl">CASE2</h3>
							<ul class="c-list02__cate">
								<li>TOP</li>
								<li>コンセプト</li>
								<li>サービス・商品について</li>
								<li>サービスプラン</li>
								<li>FAQ</li>
								<li>ニュース/詳細</li>
								<li>お問い合わせ/完了</li>
								<li>プライバシーポリシー</li>
								<li>サイトマップ</li>
								<li>404</li>
							</ul>
							<p class="c-list02__txt">コンセプトや料金プランなどページを独立させたサービスサイト。<br>また、CMSとして、ニュースや実績を更新することも可能です。</p>
							<ul class="c-list02__quote">
								<li>3~4ヶ月</li>
								<li>230万〜</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-top05">
		<div class="l-container">
			<div class="c-title01 c-title01--center js-view js-title">
				<span class="c-title01__en">Contact</span>
				<h3 class="c-title01__txt">お問い合わせについて</h3>
			</div>
			<p class="p-top05__txt01 js-view js-fade">ANVIEではデザイナーおよびエンジニアの<br>熱量のこもった高いクオリティーのWeb制作を提供します。</p>
			<div class="p-top05__box js-view js-fade">
				<h3 class="p-top05__ttl">こんな方におすすめです。</h3>
				<ul class="p-top05__txt02">
					<li>・大手制作会社は高すぎるため、躊躇してしまっている方</li>
					<li>・自社サイトの見栄えが良くないため、社内外から不満の声が上がっている方</li>
					<li>・問い合わせ数や口コミなど成果にコミットする制作会社を探している方</li>
					<li>・先進的でかつUXの高いWebサイトを制作したい方</li>
					<li>・シンプルにかっこいいWebサイトを作りたい方</li>
				</ul>
			</div>
		</div>
		<div class="c-contact">
			<div class="c-contact__wave">
				<div class="c-contact__waveItem"></div>
				<div class="c-contact__waveItem"></div>
			</div>
			<div class="c-contact__text" id="contact">
				<h3 class="c-contact__ttl">お問い合わせ</h3>
				<p class="c-contact__txt">※お問い合わせいただいた当日中にご返信いたします<br>※企画・打ち合わせ、見積もりは無料ですので、<br>お気軽にお問い合わせくださいませ。</p>
			</div>
			<div class="c-form">
				<div class="c-form__row">
					<div class="c-form__item">
						<label class="c-form__label">お名前<abbr title="required">*</abbr></label>
						<input type="text" class="c-form__input" id="name" name="" placeholder="お名前">
					</div>
					<div class="c-form__item">
						<label class="c-form__label">メールアドレス<abbr title="required">*</abbr></label>
						<input type="email" class="c-form__input" id="email" name="" placeholder="exampla@example.com">
					</div>
				</div>
				<div class="c-form__row">
					<div class="c-form__item">
						<label class="c-form__label">会社名/個人名義<abbr title="required">*</abbr></label>
						<input type="text" class="c-form__input" id="company" name="" placeholder="会社名/個人名義">
					</div>
					<div class="c-form__item">
						<label class="c-form__label">お問い合わせカテゴリ<abbr title="required">*</abbr></label>
						<select name="" class="c-form__input c-form__input--select">
							<optgroup label="">
								<option value="" disabled="">お問い合わせカテゴリ</option>
								<option value="">キャンペーン/LPサイト制作について</option>
								<option value="">コーポレートサイト制作について</option>
								<option value="">サービスサイト制作について</option>
								<option value="">よくわからないので相談したい</option>
								<option value="">その他</option>
							</optgroup>
						</select>
					</div>
				</div>
				<div class="c-form__row c-form__row--textarea">
					<label class="c-form__label">お問い合わせ内容<abbr title="required">*</abbr></label>
					<textarea class="c-form__input c-form__input--textarea" name=""></textarea>
				</div>
				<div class="c-form__btn">
					<span class="c-form__submit">送信内容確認</span>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
